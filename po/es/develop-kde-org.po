#
# Eloy Cuadra <ecuadra@eloihr.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: develop-kde-org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:19+0000\n"
"PO-Revision-Date: 2022-01-05 19:47+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: config.yaml:0
msgid "API"
msgstr "API"

#: i18n/en.yaml:0
msgid "Previous"
msgstr "Anterior"

#: i18n/en.yaml:0
msgid "Next"
msgstr "Siguiente"

#: i18n/en.yaml:0
msgid "Read more"
msgstr "Leer más"

#: i18n/en.yaml:0
msgid "Search this site…"
msgstr "Buscar este sitio..."

#: i18n/en.yaml:0
msgid "in"
msgstr "en"

#: i18n/en.yaml:0
msgid "All Rights Reserved"
msgstr "Todos los derechos reservados"

#: i18n/en.yaml:0
msgid "Privacy Policy"
msgstr "Política de privacidad"

#: i18n/en.yaml:0
msgid "By"
msgstr "Por"

#: i18n/en.yaml:0
msgid "Created"
msgstr "Creado"

#: i18n/en.yaml:0
msgid "Last modified"
msgstr "Última modificación"

#: i18n/en.yaml:0
msgid "Edit this page"
msgstr "Editar esta página"

#: i18n/en.yaml:0
msgid "Create documentation issue"
msgstr "Crear problema de documentación"

#: i18n/en.yaml:0
msgid "Create project issue"
msgstr "Crear problema de proyecto"

#: i18n/en.yaml:0
msgid "Posts in"
msgstr "Publicaciones en"

#: i18n/en.yaml:0
msgid "Icons"
msgstr "Iconos"

#: i18n/en.yaml:0
msgid "Categories"
msgstr "Categorías"

#: i18n/en.yaml:0
msgid "Search for icons"
msgstr "Buscar iconos"

#: i18n/en.yaml:0
msgid "Start typing to filter..."
msgstr "Empiece a escribir para filtrar..."

#: i18n/en.yaml:0
msgid "Nothing found for this category/keyword combination."
msgstr ""
"No se ha encontrado nada para esta combinación de categoría y palabra clave."

#: i18n/en.yaml:0
msgid "KDE Developer Platform"
msgstr "Plataforma de desarrollo de KDE"

#: i18n/en.yaml:0
msgid ""
"Design, build and distribute beautiful, usable applications with KDE "
"technologies."
msgstr ""
"Diseñe, compile y distribuya aplicaciones atractivas y usables con las "
"tecnologías de KDE."

#: i18n/en.yaml:0
msgid "Multi-Platform"
msgstr "Multiplataforma"

#: i18n/en.yaml:0
msgid ""
"Develop once, deploy everywhere. Built on top of Qt, KDE's technologies work "
"on every platform."
msgstr ""
"Desarrolle una vez, implemente en todas partes. Construidas sobre Qt, las "
"tecnologías de KDE funcionan en todas las plataformas."

#: i18n/en.yaml:0
msgid "Desktop Linux, Android, Windows, macOS, embedded, and more"
msgstr "Escritorio de Linux, Android, Window, macOS, integradas y más"

#: i18n/en.yaml:0
msgid "Plasma running on a phone, laptop and TV"
msgstr "Plasma funcionando en un teléfono, un portátil y en un televisor"

#: i18n/en.yaml:0
msgid "KDE Frameworks: Enhance the Qt Experience"
msgstr "KDE Frameworks: Mejora la experiencia de Qt"

#: i18n/en.yaml:0
msgid ""
"KDE Frameworks cover 80 add-on libraries for programming with Qt. All "
"libraries have been well-tested in real world scenarios and are "
"comprehensively documented. KDE's libraries are distributed under LGPL or "
"MIT licenses."
msgstr ""
"KDE Frameworks contiene 80 bibliotecas complementarias para programar con "
"Qt. Todas las bibliotecas se han probado minuciosamente en escenarios del "
"mundo real y están exhaustivamente documentadas. Las bibliotecas de KDE se "
"distribuyen bajo las licencias LGPL o MIT."

#: i18n/en.yaml:0
msgid "Kirigami UI Framework"
msgstr "Framework de interfaz de usuario Kirigami"

#: i18n/en.yaml:0
msgid "Kirigami"
msgstr "Kirigami"

#: i18n/en.yaml:0
msgid ""
"Build Beautiful, Convergent Apps that Run on Phones, TVs and Everything in "
"Between."
msgstr ""
"Cree aplicaciones atractivas y convergentes que se ejecuten en teléfonos, "
"televisores y en cualquier cosa entre ambos."

#: i18n/en.yaml:0
msgid ""
"The line between desktop and mobile is blurring and users expect the same "
"quality experience on every device. Applications using Kirigami adapt "
"brilliantly to mobile, desktop, TVs, infortainment systems and everything in "
"between."
msgstr ""
"La línea entre escritorio y móvil se está diluyendo y los usuarios esperan "
"la misma calidad de experiencia en cualquier dispositivo. Las aplicaciones "
"que usan Kirigami se adaptan de una forma brillante al móvil, el escritorio, "
"el televisor, sistemas de infoentretenimiento y cualquier cosa entre ellos."

#: i18n/en.yaml:0
msgid ""
"Kirigami's components are goodlooking and consistent, and Kirigami itself "
"provides a clearly defined workflow. Users of Kirigami apps will appreciate "
"the smart choices made in the API and uncluttered design."
msgstr ""
"Los componentes de Kirigami tienen buen aspecto y son consistentes, mientras "
"que el propio Kirigami proporciona un flujo de trabajo claramente definido. "
"Los usuarios de aplicaciones Kirigami apreciarán las inteligentes elecciones "
"realizadas en la API y el despejado diseño."

#: i18n/en.yaml:0
msgid "Discover Kirigami"
msgstr "Descubra Kirigami"

#: i18n/en.yaml:0
msgid "KDevelop"
msgstr "KDevelop"

#: i18n/en.yaml:0
msgid "A cross-platform IDE for C, C++, Python, QML/JavaScript and PHP"
msgstr "Un IDE multiplataforma para C, C++, Python, QML/JavaScript y PHP"

#: i18n/en.yaml:0
msgid ""
"Open Source, powerful and fast, KDevelop  offers a seamless development "
"environment to programmers that work on projects of any size. KDevelop helps "
"you get the job done while staying out of your way."
msgstr ""
"De código abierto, potente y rápido, KDevelop ofrece un entorno de "
"desarrollo perfecto para los programadores que trabajan en proyectos de "
"cualquier tamaño. KDevelop le ayuda a realizar su trabajo sin estorbarle."

#: i18n/en.yaml:0
msgid "Get KDevelop"
msgstr "Obtener KDevelop"

#: i18n/en.yaml:0
msgid "Documentation"
msgstr "Documentación"

#: i18n/en.yaml:0
msgid ""
"From beginners to experienced Qt developers, here is all you will need to "
"know to start developing KDE applications."
msgstr ""
"Desde principiantes hasta experimentados desarrolladores de Qt, esto es todo "
"lo que necesita para saber cómo empezar a desarrollar aplicaciones de KDE."

#: i18n/en.yaml:0
msgid "Design"
msgstr "Diseño"

#: i18n/en.yaml:0
msgid "Learn about KDE's design guidelines for apps and icons"
msgstr "Conozca las directrices de diseño de KDE para aplicaciones e iconos."

#: i18n/en.yaml:0
msgid "Develop"
msgstr "Desarrollo"

#: i18n/en.yaml:0
msgid "Check out the tools and libraries that help you build KDE apps"
msgstr ""
"Consulte las herramientas y bibliotecas que le ayudarán a crear aplicaciones "
"de KDE."

#: i18n/en.yaml:0
msgid "Distribute"
msgstr "Distribución"

#: i18n/en.yaml:0
msgid "Get your application in front of users"
msgstr "Ponga su aplicación al alcance de los usuarios."
