#
# SPDX-FileCopyrightText: 2024 Kisaragi Hiu <mail@kisaragi-hiu.com>
msgid ""
msgstr ""
"Project-Id-Version: documentation-develop-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:19+0000\n"
"PO-Revision-Date: 2024-03-03 02:21+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Traditional Chinese <zh-l10n@lists.slat.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"SPDX-FileCopyrightText: FULL NAME <EMAIL@ADDRESS>\n"
"SPDX-License-Identifier: LGPL-3.0-or-later\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 24.04.70\n"

#: content/hig/_index.md:0
msgid "KDE Human Interface Guidelines"
msgstr "KDE 介面設計指南"

#: content/hig/_index.md:9
msgid ""
"The **KDE Human Interface Guidelines (HIG)** offer designers and developers "
"a set of recommendations for producing beautiful, usable, and consistent "
"user interfaces for convergent desktop and mobile applications and workspace "
"widgets. Our aim is to improve the experience for users by making consistent "
"interfaces for desktop, mobile, and everything in between, more consistent, "
"intuitive and learnable."
msgstr ""
"**KDE 介面設計指南 (Human Interface Guidelines)** 提供建議，讓設計師和開發者"
"能夠為響應式桌面/行動應用程式及小元件製作漂亮、好用且一致的使用者介面。我們的"
"目標是讓響應式介面更加直覺且易學來改善使用者的體驗。"

#: content/hig/_index.md:15
msgid "Design Vision"
msgstr "設計理念"

#: content/hig/_index.md:18
msgid ""
"Our design vision focuses on two attributes of KDE software that connect its "
"future to its history:"
msgstr "我們的設計理念著重於兩個 KDE 軟體將未來連結到歷史的要素："

#: content/hig/_index.md:21
msgid ""
"![Simple by default, powerful when needed.](/hig/HIGDesignVisionFullBleed."
"png)"
msgstr "![預設簡潔，需要時足夠強大。](/hig/HIGDesignVisionFullBleed.png)"

#: content/hig/_index.md:23
msgid "Simple by default..."
msgstr "預設簡潔..."

#: content/hig/_index.md:25
msgid ""
"*Simple and inviting. KDE software is pleasant to experience and easy to use."
"*"
msgstr "*簡潔且平易近人。KDE 軟體體驗起來令人愉快且容易使用。*"

#: content/hig/_index.md:28
msgid "**Make it easy to focus on what matters**"
msgstr "**讓使用者能聚焦於重要的事情**"

#: content/hig/_index.md:30
msgid ""
"Remove or minimize elements not crucial to the primary or main task. Use "
"spacing to keep things organized. Use color to draw attention. Reveal "
"additional information or optional functions only when needed."
msgstr ""

#: content/hig/_index.md:35
msgid "**\"I know how to do that!\"**"
msgstr "**「我知道要怎麼做這件事！」**"

#: content/hig/_index.md:37
msgid ""
"Make things easier to learn by reusing design patterns from other "
"applications. Other applications that use good design are a precedent to "
"follow."
msgstr ""

#: content/hig/_index.md:41
msgid "**Do the heavy lifting for me**"
msgstr ""

#: content/hig/_index.md:43
msgid ""
"Make complex tasks simple. Make novices feel like experts. Create ways in "
"which your users can naturally feel empowered by your software."
msgstr ""

#: content/hig/_index.md:47
msgid "...Powerful when needed"
msgstr "...需要時足夠強大"

#: content/hig/_index.md:49
msgid ""
"*Power and flexibility. KDE software allows users to be effortlessly "
"creative and efficiently productive.*"
msgstr ""

#: content/hig/_index.md:52
msgid "**Solve a problem**"
msgstr "**解決問題**"

#: content/hig/_index.md:54
msgid ""
"Identify and make very clear to the user what need is addressed and how."
msgstr ""

#: content/hig/_index.md:57
msgid "**Always in control**"
msgstr ""

#: content/hig/_index.md:59
msgid ""
"It should always be clear what can be done, what is currently happening, and "
"what has just happened. The user should never feel at the mercy of the tool. "
"Give the user the final say."
msgstr ""

#: content/hig/_index.md:64
msgid "**Be flexible**"
msgstr "**有彈性**"

#: content/hig/_index.md:66
msgid ""
"Provide sensible defaults but consider optional functionality and "
"customization options that don\\'t interfere with the primary task."
msgstr ""

#: content/hig/_index.md:70
msgid "Note"
msgstr "注意"

#: content/hig/_index.md:72
msgid ""
"KDE encourages developing and designing for customization, while providing "
"good default settings. Integrating into other desktop environments is also a "
"virtue, but ultimately we aim for perfection within our own Plasma desktop "
"environment with the default themes and settings. This aim should not be "
"compromised."
msgstr ""
